import SNS from 'sns-mobile';
import ValidationUtil from './lib/ValidationUtil';

let snsApp = null;

class AmazonSNSDispatcher {

  /**
  * @param amazonSNSConfiguration {AmazonSNSConfiguration}
  */
  constructor(amazonSNSConfiguration) {

    ValidationUtil.notAnObjectOrNullOrUndefined(amazonSNSConfiguration);
    ValidationUtil.allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration);
    ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration);

    this.isIOS = (Object.is(amazonSNSConfiguration.getPlatform().toLowerCase(), 'ios'));

    this.amazonSNSConfiguration = amazonSNSConfiguration;

    snsApp = new SNS({
      platform: this.amazonSNSConfiguration.getPlatform(),
      region: this.amazonSNSConfiguration.getRegion(),
      accessKeyId: this.amazonSNSConfiguration.getApiKeyId(),
      secretAccessKey: this.amazonSNSConfiguration.getApiSecretKey(),
      platformApplicationArn: this.amazonSNSConfiguration.getPlatformApplicationArn(),
    });
  }

  sendData(deviceId, title, message) {
      return (
        this.addUser(deviceId, deviceId)
          .then(arn => this.sendMessage(arn, title, message))
          .then(this.deleteUser(deviceId))
      );
  }

  addUser(deviceId, customerNumber) {
    return new Promise((fullfill, reject) => {
      snsApp.addUser(deviceId, customerNumber, (err, arn) => {
        if(err) {
          reject(err);
        } else {
          fullfill(arn);
        }
      });
    });
  }

  sendMessage(deviceId, title, message) {

    const payload = {};

    if (this.isIOS) {
      Object.assign(payload, {
        title,
        message,
        [this.amazonSNSConfiguration.getMessageKey()]: {
          aps: {
            alert: `${message}`,
            sound: 'default'
          }
        }
      });
    } else {
      Object.assign(payload, {
        GCM: {
          data: {
            payload: {
              android: {
                title: `${title}`,
                alert: `${message}`,
                icon: 'appicon',
                vibrate: true
              }
            }
          }
        }
      });
    }

    return new Promise((fulfill, reject) => {
      snsApp.sendMessage(deviceId, payload, (err, messageId) => {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          console.log(`Push message sent, ID was: ${messageId} to ${deviceId}`);
          fulfill(messageId);
        }
      });
    });
  }

  deleteUser(deviceId) {
    return new Promise((fulfill, reject) => {
      snsApp.deleteUser(deviceId, (err, res) => {
        if (err) {
          reject(err);
        } else {
          fulfill(res);
        }
      })
    });
  }
}

export default AmazonSNSDispatcher;
