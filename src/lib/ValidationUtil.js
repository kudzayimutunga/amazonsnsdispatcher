import ObjectHelperUtil from './ObjectHelperUtil';

class ValidationUtil {

    static getRequiredFields() {
        return ['platform', 'region', 'apiKeyId', 'apiSecretKey', 'platformApplicationArn', 'messageKey'];
    }

    static notAnObjectOrNullOrUndefined(amazonSNSConfiguration) {
        if (!ObjectHelperUtil.notNullOrUndefined(amazonSNSConfiguration) || typeof amazonSNSConfiguration !== 'object') {
            let error = new Error(`Property type mismatch. Expected {${this.getRequiredFields().join()}}`);
            console.log(error.message);
            throw error;
        }
        return true;
    }

    static allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration) {
        this.getRequiredFields().forEach((field) => {
            if (!amazonSNSConfiguration.hasOwnProperty(field)) {
                let error = new Error(`Property type mismatch. Expected {${field}}`);
                console.log(error.message);
                throw error;
            }
        });
        return true;
    }

    static allFieldsHaveBeenPopulated(amazonSNSConfiguration) {
        this.getRequiredFields().forEach((field) => this.isFieldPopulated(field, amazonSNSConfiguration[field]));
        return true;
    }

    static isFieldPopulated(fieldName, value) {
        if (!ObjectHelperUtil.notNullOrUndefined(value)) {
            let error = new Error(`NullorUndefined field assignment for {${fieldName}}`);
            console.log(error.message);
            throw error;
        } else if (Object.is(value, '')) {
            let error = new Error(`Unassigned Property. Expected {${fieldName}}`);
            console.log(error.message);
            throw error;
        }
        return true;
    }
}

export default ValidationUtil;
