class ObjectHelperUtil {
  static notNullOrUndefined(value) {
    if (Object.is(value, null) || Object.is(value, undefined)) {
      return false;
    }
    return true;
  }
}

export default ObjectHelperUtil;
