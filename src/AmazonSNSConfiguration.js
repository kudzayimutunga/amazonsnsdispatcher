class AmazonSNSConfiguration {

  constructor() {
    this.apiKeyId = null;
    this.apiSecretKey = null;
    this.platformApplicationArn = null;
    this.messageKey = null,
    this.platform = null;
    this.region = null;
  }

  getApiKeyId() {
    return this.apiKeyId;
  }

  setApiKeyId(keyId) {
    this.apiKeyId = keyId;
  }

  getApiSecretKey() {
    return this.apiSecretKey;
  }

  setApiSecretKey(key) {
    this.apiSecretKey = key;
  }

  getPlatformApplicationArn () {
    return this.platformApplicationArn;
  }

  setPlatformApplicationArn (arn) {
    this.platformApplicationArn = arn;
  }

  getPlatform () {
    return this.platform;
  }

  setPlatform (platform) {
    this.platform = platform;
  }

  getRegion () {
    return this.region;
  }

  setRegion (regionCode) {
    this.region = regionCode;
  }

  getMessageKey() {
    return this.messageKey;
  }

  setMessageKey(key) {
    this.messageKey = key;
  }
}

export default AmazonSNSConfiguration;
