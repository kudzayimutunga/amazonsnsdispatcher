import ValidationUtil from '../../src/lib/ValidationUtil';
import ValidationUtilSpy from '../lib/spies/validationUtilSpy';
import AmazonSNSConfiguration from '../../src/AmazonSNSConfiguration';

describe('ValidationUtil', () => {
    let amazonSNSConfiguration;
amazonSNSConfiguration
    beforeEach(() => {
        amazonSNSConfiguration = new AmazonSNSConfiguration();
        amazonSNSConfiguration.setPlatform('ios');
        amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
        amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company');
        amazonSNSConfiguration.setMessageKey('APNS_SANDBOX');
    });

    it('exposes the ValidationUtil class', () => {
        expect(ValidationUtil.getRequiredFields).toBeDefined();
        expect(ValidationUtil.notAnObjectOrNullOrUndefined).toBeDefined();
        expect(ValidationUtil.allExpectedFieldsHaveBeenReceived).toBeDefined();
        expect(ValidationUtil.allFieldsHaveBeenPopulated).toBeDefined();
        expect(ValidationUtil.isFieldPopulated).toBeDefined();
    });

    it('getRequiredFields returns an array of required fields', () => {
        spyOn(ValidationUtil, 'getRequiredFields').and.callThrough();

        ValidationUtil.getRequiredFields(amazonSNSConfiguration);
        expect(ValidationUtil.getRequiredFields).toHaveBeenCalledWith(jasmine.any(Object));
        expect(ValidationUtil.getRequiredFields(amazonSNSConfiguration)).toEqual(['platform', 'region', 'apiKeyId', 'apiSecretKey', 'platformApplicationArn', 'messageKey']);
    });

    it('returns true when notAnObjectOrNullOrUndefined is passed a true object', () => {
        spyOn(ValidationUtil, 'notAnObjectOrNullOrUndefined').and.callThrough();

        ValidationUtil.notAnObjectOrNullOrUndefined(amazonSNSConfiguration);
        expect(ValidationUtil.notAnObjectOrNullOrUndefined).toHaveBeenCalledWith(jasmine.any(Object));
        expect(ValidationUtil.notAnObjectOrNullOrUndefined(amazonSNSConfiguration)).toEqual(true);
    });

    it('throws an error when notAnObjectOrNullOrUndefined is passed a non or null or undefined object', () => {
        spyOn(ValidationUtil, 'notAnObjectOrNullOrUndefined').and.callThrough();

        expect(() => {
            ValidationUtil.notAnObjectOrNullOrUndefined("parm");
        }).toThrow(new Error('Property type mismatch. Expected {platform,region,apiKeyId,apiSecretKey,platformApplicationArn,messageKey}'));
        expect(() => {
            ValidationUtil.notAnObjectOrNullOrUndefined(null);
        }).toThrow(new Error('Property type mismatch. Expected {platform,region,apiKeyId,apiSecretKey,platformApplicationArn,messageKey}'));
        expect(() => {
            ValidationUtil.notAnObjectOrNullOrUndefined(undefined);
        }).toThrow(new Error('Property type mismatch. Expected {platform,region,apiKeyId,apiSecretKey,platformApplicationArn,messageKey}'));
    });

    it('should return true when allExpectedFieldsHaveBeenReceived', () => {
        spyOn(ValidationUtil, 'allExpectedFieldsHaveBeenReceived').and.callThrough();

        ValidationUtil.allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration);
        expect(ValidationUtil.allExpectedFieldsHaveBeenReceived).toHaveBeenCalledWith(jasmine.any(Object));
        expect(ValidationUtil.allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration)).toEqual(true);
    });

    it('should throw an exception when allExpectedFieldsHaveBeenReceived has fewer fields than expected', () => {
        expect(() => {
            class AmazonSNSConfiguration {
                constructor() {
                    this.platform
                    this.region
                    this.apiKeyId
                    this.apiSecretKey
                    this.platformApplicationArn
                }
            }
            amazonSNSConfiguration = new AmazonSNSConfiguration();
            amazonSNSConfiguration.platform = 'ios';
            amazonSNSConfiguration.region = 'eu-west-1';
            amazonSNSConfiguration.apiKeyId = 'ABDCDEFHIJKLMOPQRS';
            amazonSNSConfiguration.apiSecretKey = '789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy';
            amazonSNSConfiguration.platformApplicationArn = 'arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company';

            ValidationUtil.allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration);

        }).toThrow(new Error('Property type mismatch. Expected {messageKey}'));
    });


    it('should return true when allFieldsHaveBeenPopulated', () => {
        amazonSNSConfiguration.setPlatform('ios');
        amazonSNSConfiguration.setRegion('eu-west-1');
        amazonSNSConfiguration.setApiKeyId('ABDCDEFHIJKLMOPQRS');
        amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
        amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company');

        spyOn(ValidationUtil, 'allFieldsHaveBeenPopulated').and.callThrough();
        spyOn(ValidationUtil, 'isFieldPopulated').and.callThrough();

        ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration);
        expect(ValidationUtil.allFieldsHaveBeenPopulated).toHaveBeenCalledWith(jasmine.any(Object));

        ['platform', 'region', 'apiKeyId', 'apiSecretKey', 'platformApplicationArn', 'messageKey'].forEach((field) => {
            expect(ValidationUtil.isFieldPopulated).toHaveBeenCalledWith(field, amazonSNSConfiguration[field]);
        });
        console.log(amazonSNSConfiguration);
        expect(ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration)).toEqual(true);
    });

    it('should throw an exception when an allFieldsHaveBeenPopulated field holds an undefined or null or empty string value', () => {
        expect(() => {
            amazonSNSConfiguration = new AmazonSNSConfiguration();
            amazonSNSConfiguration.setPlatform('ios');
            amazonSNSConfiguration.setRegion('eu-west-1');
            amazonSNSConfiguration.setApiKeyId(null);
            amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
            amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company');
            amazonSNSConfiguration.setMessageKey('APNS_SANDBOX');

            ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration);

        }).toThrow(new Error('NullorUndefined field assignment for {apiKeyId}'));

        expect(() => {
            amazonSNSConfiguration = new AmazonSNSConfiguration();
            amazonSNSConfiguration.setPlatform('ios');
            amazonSNSConfiguration.setRegion('eu-west-1');
            amazonSNSConfiguration.setApiKeyId('');
            amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
            amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company');
            amazonSNSConfiguration.setMessageKey('APNS_SANDBOX');

            ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration);

        }).toThrow(new Error('Unassigned Property. Expected {apiKeyId}'));
    });

    it('should return true when isFieldPopulated', () => {
        spyOn(ValidationUtil, 'isFieldPopulated').and.callThrough();

        ValidationUtil.isFieldPopulated('apiSecretKey', 'ABDCDEFHIJKLMOPQRS');
        expect(ValidationUtil.isFieldPopulated).toHaveBeenCalledWith('apiSecretKey', 'ABDCDEFHIJKLMOPQRS');

        expect(ValidationUtil.isFieldPopulated('apiSecretKey', 'ABDCDEFHIJKLMOPQRS')).toEqual(true);
    });

    it('should throw an exception when an isFieldPopulated field holds an undefined or null or empty string value', () => {
        expect(() => {
            ValidationUtil.isFieldPopulated('apiSecretKey');
        }).toThrow(new Error('NullorUndefined field assignment for {apiSecretKey}'));

        expect(() => {
            ValidationUtil.isFieldPopulated('apiSecretKey', '');
        }).toThrow(new Error('Unassigned Property. Expected {apiSecretKey}'));

    });
});
