const proxyquire = require('proxyquire').noCallThru();

import AmazonSNSConfiguration from '../../src/AmazonSNSConfiguration';
import ValidationUtil from '../../src/lib/ValidationUtil';
import ObjectHelperUtil from '../../src/lib/ObjectHelperUtil';

describe('AmazonSNSDispatcher', () => {

    let AmazonSNSDispatcher = null,
        amazonSNSDispatcher = null,
        amazonSNSConfiguration = null,
        snsMobileSpy = null;

    beforeEach(() => {

        snsMobileSpy = jasmine.createSpyObj('snsMobileSpy', [
            'addUser',
            'sendMessage',
            'deleteUser',
            'then',
            'done',
            'allSettled'
        ]);

        AmazonSNSDispatcher = proxyquire('../../src/AmazonSNSDispatcher', {
            'sns-mobile': () => snsMobileSpy
        }).default;

        amazonSNSConfiguration = new AmazonSNSConfiguration();
        amazonSNSConfiguration.setPlatform('ios');
        amazonSNSConfiguration.setRegion('eu-west-1');
        amazonSNSConfiguration.setApiKeyId('ABDCDEFHIJKLMOPQRS');
        amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
        amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:094975918108:app/APNS_SANDBOX/com.company.company');
        amazonSNSConfiguration.setMessageKey('APNS_SANDBOX');
        amazonSNSDispatcher = new AmazonSNSDispatcher(amazonSNSConfiguration);

    });

    afterEach(() => {
        AmazonSNSDispatcher = null,
            amazonSNSDispatcher = null,
            amazonSNSConfiguration = null,
            snsMobileSpy = null;
    });

    it('run the ValidationUtil methods', () => {
        spyOn(ValidationUtil, 'notAnObjectOrNullOrUndefined').and.callThrough();
        spyOn(ValidationUtil, 'allExpectedFieldsHaveBeenReceived').and.callThrough();
        spyOn(ValidationUtil, 'allFieldsHaveBeenPopulated').and.callThrough();

        ValidationUtil.notAnObjectOrNullOrUndefined(amazonSNSConfiguration);
        expect(ValidationUtil.notAnObjectOrNullOrUndefined).toHaveBeenCalledWith(jasmine.any(Object));

        ValidationUtil.allExpectedFieldsHaveBeenReceived(amazonSNSConfiguration);
        expect(ValidationUtil.allExpectedFieldsHaveBeenReceived).toHaveBeenCalledWith(jasmine.any(Object));

        ValidationUtil.allFieldsHaveBeenPopulated(amazonSNSConfiguration);
        expect(ValidationUtil.allFieldsHaveBeenPopulated).toHaveBeenCalledWith(jasmine.any(Object));
    });

    it('exposes the class', () => {
        expect(amazonSNSDispatcher.sendData).toBeDefined();
    });

    it('adds user and completes with no error', (done) => {
        let addUserPromise = amazonSNSDispatcher.addUser('deviceId', 'deviceId');

        addUserPromise.then(() => {
            expect(snsMobileSpy.addUser).toHaveBeenCalledWith('deviceId', 'deviceId', jasmine.any(Function));
            done();
        });

        snsMobileSpy.addUser.calls.argsFor(0)[2](null, 'arn');
    });

    it('adds user and completes with an error', (done) => {
        let addUserPromise = amazonSNSDispatcher.addUser('deviceId', 'deviceId');

        addUserPromise.then((success) => {

        }, (failure) => {
            expect(snsMobileSpy.addUser).toHaveBeenCalledWith('deviceId', 'deviceId', jasmine.any(Function));
            done();
        });

        snsMobileSpy.addUser.calls.argsFor(0)[2]({
            error: true
        }, null);
    });


    it('sends a message for IOS with no error', (done) => {
        let sendMessagePromise = amazonSNSDispatcher.sendMessage('deviceId', 'title', 'message');

        sendMessagePromise.then(() => {
            expect(snsMobileSpy.sendMessage).toHaveBeenCalledWith('deviceId', jasmine.any(Object), jasmine.any(Function));
            done();
        });

        snsMobileSpy.sendMessage.calls.argsFor(0)[2](null, 'messageId');
    });

    it('sends a message for Android with no error', (done) => {

        amazonSNSConfiguration = new AmazonSNSConfiguration();
        amazonSNSConfiguration.setPlatform('android');
        amazonSNSConfiguration.setRegion('eu-west-1');
        amazonSNSConfiguration.setApiKeyId('ABDCDEFHIJKLMOPQRS');
        amazonSNSConfiguration.setApiSecretKey('789A2EnSbJy789A2EnSbJy789A2EnSbJy789A2EnSbJy');
        amazonSNSConfiguration.setPlatformApplicationArn('arn:aws:sns:eu-west-1:9999999999:app/GCM/GCMTest');
        amazonSNSConfiguration.setMessageKey('droid');

        amazonSNSDispatcher = new AmazonSNSDispatcher(amazonSNSConfiguration);
        let sendMessagePromise = amazonSNSDispatcher.sendMessage('deviceId', 'title', 'message');

        sendMessagePromise.then(() => {
            expect(snsMobileSpy.sendMessage).toHaveBeenCalledWith(
                'deviceId',
                jasmine.any(Object),
                jasmine.any(Function)
            );
            done();
        });

        snsMobileSpy.sendMessage.calls.argsFor(0)[2](null, 'messageId');
    });

    it('sends a message and errors', (done) => {
        let sendMessagePromise = amazonSNSDispatcher.sendMessage('deviceId', 'title', 'message');

        sendMessagePromise.then(() => {

        }, () => {
            expect(snsMobileSpy.sendMessage).toHaveBeenCalledWith(
                'deviceId',
                jasmine.any(Object),
                jasmine.any(Function)
            );
            done();
        });

        snsMobileSpy.sendMessage.calls.argsFor(0)[2]({
            error: true
        }, null);
    });

    it('deletes a user with no error', (done) => {
        let deleteUserPromise = amazonSNSDispatcher.deleteUser('arn');

        deleteUserPromise.then(() => {
            expect(snsMobileSpy.deleteUser).toHaveBeenCalledWith('arn', jasmine.any(Function));
            done();
        });

        snsMobileSpy.deleteUser.calls.argsFor(0)[1](null, 'messageId');
    });

    it('deletes a user and errors', (done) => {
        let deleteUserPromise = amazonSNSDispatcher.deleteUser('arn');

        deleteUserPromise.then(() => {

        }, (failure) => {
            expect(snsMobileSpy.deleteUser).toHaveBeenCalledWith('arn', jasmine.any(Function));
            done();
        });

        snsMobileSpy.deleteUser.calls.argsFor(0)[1]({
            error: true
        }, null);
    });

    it('sends data completes with no error', (done) => {

        snsMobileSpy.addUser.and.callFake((arn, payload, func) => {
            setTimeout(() => {
                func(null, 'arn');
            }, 2000);
        });

        snsMobileSpy.sendMessage.and.callFake((arn, payload, func) => {
            setTimeout(() => {
                func(null, 'messageId');
            }, 2000);
        });

        snsMobileSpy.deleteUser.and.callFake((messageId, func) => {
            setTimeout(() => {
                func(null, 'result');
            }, 2000);
        });

        amazonSNSDispatcher.sendData('deviceId', 'title', 'message').then((success) => {
            expect(snsMobileSpy.addUser).toHaveBeenCalledWith('deviceId', 'deviceId', jasmine.any(Function));
        }).then(() => {
            expect(snsMobileSpy.sendMessage).toHaveBeenCalledWith('arn', jasmine.any(Object), jasmine.any(Function));
        }).then(() => {
            expect(snsMobileSpy.deleteUser).toHaveBeenCalledWith('deviceId', jasmine.any(Function));
            done();
        });
    });

    it('sends data completes with error', (done) => {

        snsMobileSpy.addUser.and.callFake((arn, payload, func) => {
            setTimeout(() => {
                func(null, 'arn');
            }, 2000);
        });

        snsMobileSpy.sendMessage.and.callFake((arn, payload, func) => {
            setTimeout(() => {
                func({
                    error: true
                }, null);
            }, 2000);
        });

        snsMobileSpy.deleteUser.and.callFake((messageId, func) => {
            setTimeout(() => {
                func(null, 'result');
            }, 2000);
        });

        amazonSNSDispatcher.sendData('deviceId', 'title', 'message').then((success) => {
            expect(snsMobileSpy.addUser).toHaveBeenCalledWith('deviceId', 'deviceId', jasmine.any(Function));
        }).then(() => {

        }, () => {
            expect(snsMobileSpy.sendMessage).toHaveBeenCalledWith('arn', jasmine.any(Object), jasmine.any(Function));
        }).then(() => {
            expect(snsMobileSpy.deleteUser).toHaveBeenCalledWith('deviceId', jasmine.any(Function));
            done();
        });
    });
});
