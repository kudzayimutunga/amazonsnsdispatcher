import * as IndexModules from '../../src';
import AmazonSNSDispatcher from '../../src/AmazonSNSDispatcher';
import AmazonSNSConfiguration from '../../src/AmazonSNSConfiguration';

describe('Index', () => {
  it('exposes the AmazonSNSDispatcher and configuration classes', () => {
    expect(IndexModules.AmazonSNSDispatcher).toEqual(AmazonSNSDispatcher);
    expect(IndexModules.AmazonSNSConfiguration).toEqual(AmazonSNSConfiguration);
  });
});
