import AmazonSNSConfiguration from '../../src/AmazonSNSConfiguration';

describe('AmazonSNSConfiguration', () => {
    let amazonSNSConfig;
    beforeEach(() => {
        amazonSNSConfig = new AmazonSNSConfiguration();
    });

    it('instantiates to an object', () => {
        expect(amazonSNSConfig).toEqual(jasmine.any(Object));
    });

    it('exposes the AmazonSNSConfiguration class', () => {
        expect(amazonSNSConfig.apiKeyId).toBeDefined();
        expect(amazonSNSConfig.apiSecretKey).toBeDefined();
        expect(amazonSNSConfig.region).toBeDefined();
        expect(amazonSNSConfig.messageKey).toBeDefined();
        expect(amazonSNSConfig.platform).toBeDefined();
        expect(amazonSNSConfig.platformApplicationArn).toBeDefined();
        expect(amazonSNSConfig.getApiKeyId).toBeDefined();
        expect(amazonSNSConfig.setApiKeyId).toBeDefined();
        expect(amazonSNSConfig.getApiSecretKey).toBeDefined();
        expect(amazonSNSConfig.setApiSecretKey).toBeDefined();
        expect(amazonSNSConfig.getPlatformApplicationArn).toBeDefined();
        expect(amazonSNSConfig.setPlatformApplicationArn).toBeDefined();
        expect(amazonSNSConfig.getPlatform).toBeDefined();
        expect(amazonSNSConfig.setPlatform).toBeDefined();
        expect(amazonSNSConfig.getRegion).toBeDefined();
        expect(amazonSNSConfig.setRegion).toBeDefined();
    });

    it('should get the ApiKeyId', () => {
        spyOn(amazonSNSConfig, 'getApiKeyId').and.callThrough();
        amazonSNSConfig.getApiKeyId();

        expect(amazonSNSConfig.getApiKeyId).toHaveBeenCalled();
    });

    it('should set the ApiKeyId', () => {
        spyOn(amazonSNSConfig, 'setApiKeyId').and.callThrough();

        amazonSNSConfig.setApiKeyId('ABCDEFGHIJKLMNO');
        expect(amazonSNSConfig.setApiKeyId).toHaveBeenCalledWith('ABCDEFGHIJKLMNO');
    });

    it('should get the messageKey', () => {
        spyOn(amazonSNSConfig, 'getMessageKey').and.callThrough();

        amazonSNSConfig.getMessageKey();
        expect(amazonSNSConfig.getMessageKey).toHaveBeenCalled();
    });

    it('should set the messageKey', () => {
        spyOn(amazonSNSConfig, 'setMessageKey').and.callThrough();

        amazonSNSConfig.setMessageKey('APNS_SANDBOX');
        expect(amazonSNSConfig.setMessageKey).toHaveBeenCalledWith('APNS_SANDBOX');
    });

    it('should get the ApiSecretKey', () => {
        spyOn(amazonSNSConfig, 'getApiSecretKey').and.callThrough();

        amazonSNSConfig.getApiSecretKey();
        expect(amazonSNSConfig.getApiSecretKey).toHaveBeenCalled();
    });

    it('should set the ApiSecretKey', () => {
        spyOn(amazonSNSConfig, 'setApiSecretKey').and.callThrough();

        amazonSNSConfig.setApiSecretKey('019283abcdefg457689hijkl');
        expect(amazonSNSConfig.setApiSecretKey).toHaveBeenCalledWith('019283abcdefg457689hijkl');
    });

    it('should get the PlatformApplicationArn', () => {
        spyOn(amazonSNSConfig, 'getPlatformApplicationArn').and.callThrough();

        amazonSNSConfig.getPlatformApplicationArn();
        expect(amazonSNSConfig.getPlatformApplicationArn).toHaveBeenCalled();
    });

    it('should set the PlatformApplicationArn', () => {

        spyOn(amazonSNSConfig, 'setPlatformApplicationArn').and.callThrough();

        amazonSNSConfig.setPlatformApplicationArn('arn:aws:sns:eu-west-1:99999999:app/APNS_SANDBOX/com.firstutility.firstutility');
        expect(amazonSNSConfig.setPlatformApplicationArn).toHaveBeenCalledWith('arn:aws:sns:eu-west-1:99999999:app/APNS_SANDBOX/com.firstutility.firstutility');
    });

    it('should get the Platform', () => {
        spyOn(amazonSNSConfig, 'getPlatform').and.callThrough();

        amazonSNSConfig.getPlatform();
        expect(amazonSNSConfig.getPlatform).toHaveBeenCalled();
    });

    it('should set the Platform', () => {
        spyOn(amazonSNSConfig, 'setPlatform').and.callThrough();

        amazonSNSConfig.setPlatform('ios');
        expect(amazonSNSConfig.setPlatform).toHaveBeenCalledWith('ios');
    });

    it('should get the Region', () => {
        spyOn(amazonSNSConfig, 'getRegion').and.callThrough();

        amazonSNSConfig.getRegion();
        expect(amazonSNSConfig.getRegion).toHaveBeenCalled();
    });

    it('should set the Region', () => {
        spyOn(amazonSNSConfig, 'setRegion').and.callThrough();

        amazonSNSConfig.setRegion('eu-west-1');
        expect(amazonSNSConfig.setRegion).toHaveBeenCalledWith('eu-west-1');
    });

});
