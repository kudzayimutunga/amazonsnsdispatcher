export default () => {
  return jasmine.createSpyObj("amazonSNSConfigSpy", [
    "getApiKeyId",
    "setApiKeyId",
    "getApiSecretKey",
    "setApiSecretKey",
    "getPlatformApplicationArn",
    "setPlatformApplicationArn",
    "getPlatform",
    "setPlatform",
    "getRegion",
    "setRegion",
    "getConfig"
  ]);
};
