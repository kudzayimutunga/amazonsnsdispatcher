export default () => {
  return jasmine.createSpyObj('ValidationUtilSpy', [
    'getRequiredFields',
    'notAnObjectOrNullOrUndefined',
    'allExpectedFieldsHaveBeenReceived',
    'allFieldsHaveBeenPopulated',
    'isFieldPopulated'
  ]);
};
