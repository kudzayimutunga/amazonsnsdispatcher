export default () => {
    return jasmine.createSpyObj("amazonSNSDispatcherSpy", ["sendData"]);
};
